<?php
/*
 * This file is part of the AdminGeneratorBundle.
 *
 * Carles Pibernat
 */

namespace CarlesPibernat\AdminGeneratorBundle\Controller;


use CarlesPibernat\AdminGeneratorBundle\Entity\Media;
use CarlesPibernat\AdminGeneratorBundle\Type\MediaType;
use CarlesPibernat\AdminGeneratorBundle\Model\AdminManager;
use CarlesPibernat\AdminGeneratorBundle\Model\Utils\Breadcrumb;
use CarlesPibernat\AdminGeneratorBundle\Model\Utils\BreadcrumbItem;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends Controller
{
    public function removeTrailingSlashAction(Request $request)
    {
        $pathInfo = $request->getPathInfo();
        $requestUri = $request->getRequestUri();

        $url = str_replace($pathInfo, rtrim($pathInfo, ' /'), $requestUri);

        return $this->redirect($url, 301);
    }

    public function homepageAction($entity)
    {
        if($entity == null)
            return $this->redirectToRoute('admin_generator_dashboard');


        return $this->redirectToRoute('admin_generator_listview', array('entity' => $entity));
    }

    public function dashboardAction()
    {
        $breadcrumb = new Breadcrumb();
        $breadcrumb->setSectionTitle($this->get('translator')->trans('admin.generator.dashboard'));

        return $this->render('@AdminGenerator/admin/dashboard.html.twig', array('breadcrumb' => $breadcrumb));
    }

    public function listViewAction($entity, Request $request)
    {

        $adminManager = $this->getAdminManager();

        $fields = $adminManager->getAdminInstance($entity)->getList();
        $class = $adminManager->getEntity($entity)->getClass();

        $dql   = "SELECT a FROM $class a ORDER BY a.id";
        $query = $this->getDoctrine()->getManager()->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );

        $breadcrumb = new Breadcrumb();
        $breadcrumb->setEntityClassLabel(new BreadcrumbItem(
            $adminManager->getEntity($entity)->getLabel()
        ));
        $breadcrumb->setSectionTitle($adminManager->getEntity($entity)->getLabel());
        $breadcrumb->setSectionDescription($this->get('translator')->trans('admin.generator.list'));

        return $this->render('@AdminGenerator/admin/listView.html.twig', array(
            'fields' => $fields,
            'entity' => $entity,
            'pagination' => $pagination,
            'entityLabel' => $adminManager->getEntity($entity)->getLabel(),
            'breadcrumb' => $breadcrumb
        ));
    }

    public function createAction($entity, Request $request)
    {
        $adminManager = $this->getAdminManager();

        $fields = $adminManager->getAdminInstance($entity)->getForm();
        $class = $adminManager->getEntity($entity)->getClass();
        $instance = new $class();

        $formBuilder = $this->createFormBuilder($instance);

        foreach ($fields as $field) {
            if ($field->getName() != 'id') {
                $formBuilder->add($field->getName(), $field->getType(), $field->getOptions());
            }
        }

        $formBuilder->add('create', SubmitType::class, array(
            'label' => 'admin.generator.create'
        ));

        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $instance = $form->getData();

            $adminManager->getAdminInstance($entity)->preCreate($instance);

            $em = $this->getDoctrine()->getManager();
            $em->persist($instance);
            $em->flush();

            return $this->redirectToRoute('admin_generator_listview', array(
                'entity' => $entity
            ));
        }

        $breadcrumb = new Breadcrumb();
        $breadcrumb
            ->setEntityClassLabel(new BreadcrumbItem(
                $adminManager->getEntity($entity)->getLabel(),
                $this->generateUrl('admin_generator_listview', array('entity' => $entity))))
            ->setEntityLabel(new BreadcrumbItem(ucfirst($this->get('translator')->trans('admin.generator.new.entry'))))
        ;
        $breadcrumb->setSectionTitle($adminManager->getEntity($entity)->getLabel());
        $breadcrumb->setSectionDescription($this->get('translator')->trans('admin.generator.new.entry'));

        return $this->render('@AdminGenerator/admin/form.html.twig', array(
            'form' => $form->createView(),
            'breadcrumb' => $breadcrumb
        ));
    }

    public function editAction($entity, $id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $adminManager = $this->getAdminManager();

        $fields = $adminManager->getAdminInstance($entity)->getForm();
        $class = $adminManager->getEntity($entity)->getClass();

        $repository = $em->getRepository($class);
        $instance = $repository->findOneById($id);

        $formBuilder = $this->createFormBuilder($instance);

        foreach ($fields as $field) {
            $formBuilder->add($field->getName(), $field->getType(), $field->getOptions());
        }

        $formBuilder->add('create', SubmitType::class, array(
            'label' => 'admin.generator.save'
        ));

        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $instance = $form->getData();

            $adminManager->getAdminInstance($entity)->preUpdate($instance);

            $em->persist($instance);
            $em->flush();

            return $this->redirectToRoute('admin_generator_listview', array(
                'entity' => $entity
            ));
        }

        $breadcrumb = new Breadcrumb();
        $breadcrumb
            ->setEntityClassLabel(new BreadcrumbItem(
                $adminManager->getEntity($entity)->getLabel(),
                $this->generateUrl('admin_generator_listview', array('entity' => $entity))))
            ->setEntityLabel(new BreadcrumbItem((string) $instance))
        ;
        $breadcrumb->setSectionTitle($adminManager->getEntity($entity)->getLabel());
        $breadcrumb->setSectionDescription($this->get('translator')->trans('admin.generator.edit'));

        return $this->render('@AdminGenerator/admin/form.html.twig', array(
            'form' => $form->createView(),
            'breadcrumb' => $breadcrumb
        ));
    }

    public function removeAction($entity, $id)
    {
        $this->removeEntity($entity, $id);

        $this->addFlash(
            'success',
            $this->get('translator')->trans("admin.generator.remove.success", array('%id%' => $id))
        );

        return $this->redirectToRoute('admin_generator_listview', array(
            'entity' => $entity
        ));
    }

    public function multipleRemoveAction($entity, Request $request)
    {
        $entities = $request->get('entities');

        foreach ($entities as $id => $element) {
            $this->removeEntity($entity, $id);
        }

        $this->addFlash(
            'success',
            $this->get('translator')->trans("admin.generator.multiple.remove.success")
        );

        return $this->redirectToRoute('admin_generator_listview', array(
            'entity' => $entity
        ));
    }

    public function mediaAction(Request $request)
    {
        $class = Media::class;
        $dql   = "SELECT a FROM $class a";
        $query = $this->getDoctrine()->getManager()->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $medias = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            30
        );

        $breadcrumb = new Breadcrumb();
        $mediaTitle = $this->get('translator')->trans("admin.generator.media");
        $breadcrumb->setSectionTitle($mediaTitle);
        $breadcrumb->setEntityClassLabel(new BreadcrumbItem(ucfirst($mediaTitle)));

        return $this->render('AdminGeneratorBundle:admin:media.html.twig', array(
            'medias' => $medias,
            'breadcrumb' => $breadcrumb
        ));
    }

    public function newMediaAction(Request $request)
    {
        $media = new Media();
        $form = $this->createForm(MediaType::class, $media);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $media->setUploadedAt(new \DateTime());

            $file = $media->getFileName();
            $fileName = md5(uniqid()).'.'.$file->guessExtension();

            $media->setMimeType($file->getMimeType());

            $file->move(
                $this->get('admin_generator.user_configuration')->getUploadDirectory(),
                $fileName
            );

            $media->setFileName($fileName);

            $em = $this->getDoctrine()->getManager();
            $em->persist($media);
            $em->flush();

            $this->addFlash(
                'success',
                ucfirst($this->get('translator')->trans("admin.generator.file.upload.success"))
            );

            return $this->redirectToRoute('admin_generator_media');
        }

        $breadcrumb = new Breadcrumb();
        $mediaTitle = $this->get('translator')->trans("admin.generator.media");
        $breadcrumb->setSectionTitle($mediaTitle);
        $breadcrumb->setSectionDescription($this->get('translator')->trans("admin.generator.media.section.description"));
        $breadcrumb->setEntityClassLabel(new BreadcrumbItem(
            ucfirst($mediaTitle),
            $this->generateUrl('admin_generator_media')
        ));
        $breadcrumb->setEntityLabel(new BreadcrumbItem(ucfirst($this->get('translator')->trans("admin.generator.new.media"))));

        return $this->render('AdminGeneratorBundle:admin:form.html.twig', array(
            'breadcrumb' => $breadcrumb,
            'form' => $form->createView()
        ));
    }

    public function removeEntity($entity, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $adminManager = $this->getAdminManager();

        $class = $adminManager->getEntity($entity)->getClass();

        $repository = $em->getRepository($class);
        $instance = $repository->findOneById($id);

        if ($instance) {
            $em->remove($instance);
            $em->flush();

        } else {
            throw $this->createNotFoundException("Entity not found");
        }
    }

    private function getAdminManager()
    {
        return new AdminManager(
            $this->get('admin_generator.user_configuration'),
            $this->getDoctrine()->getManager(),
            $this->container
        );
    }
}