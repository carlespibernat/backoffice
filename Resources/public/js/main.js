
$(function(){
    /** List view select all checkbox **/
    $('#all-checkbox-selector').change(function(){
        var checkbox = this;
        $('.select-checkbox').prop('checked', checkbox.checked);
    });
});
