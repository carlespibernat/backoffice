<?php
/*
 * This file is part of the AdminGeneratorBundle.
 *
 * Carles Pibernat
 */

namespace CarlesPibernat\AdminGeneratorBundle\Admin;

use CarlesPibernat\AdminGeneratorBundle\Entity\User;
use CarlesPibernat\AdminGeneratorBundle\Model\AbstractAdmin;
use CarlesPibernat\AdminGeneratorBundle\Model\Mapper\FormMapper;
use CarlesPibernat\AdminGeneratorBundle\Model\Mapper\ListMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Default admin for user
 *
 * @author Carles Pibernat
 */
class UserAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id',             IntegerType::class,     array('disabled' => true))
            ->add('username',       TextType::class)
            ->add('plainPassword',  PasswordType::class,    array('required' => false, 'label' => 'New password'))
            ->add('name',           TextType::class,        array('required' => false))
            ->add('lastname',       TextType::class,        array('required' => false))
            ->add('email',          EmailType::class)
            ->add('enabled',        CheckboxType::class,    array('required' => false, 'label' => 'Enabled'))
        ;
    }

    protected function configureFilters()
    {
        // TODO: Implement configureFilters() method.
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('username')
            ->add('email')
        ;
    }

    public function preUpdate($user)
    {
        if ($user->getPlainPassword()) {
            $container = $this->getContainer();
            $userManager = $container->get('fos_user.user_manager');

            $userManager->updateUser($user, true);
        }
    }
}