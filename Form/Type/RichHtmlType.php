<?php
/**
 * Created by PhpStorm.
 * User: carles
 * Date: 29/4/17
 * Time: 11:53
 */

namespace CarlesPibernat\AdminGeneratorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class RichHtmlType extends AbstractType
{
    public function getParent()
    {
        return TextType::class;
    }
}