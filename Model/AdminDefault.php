<?php
/*
 * This file is part of the AdminGeneratorBundle.
 *
 * Carles Pibernat
 */

namespace CarlesPibernat\AdminGeneratorBundle\Model;


use CarlesPibernat\AdminGeneratorBundle\Model\Mapper\FormMapper;
use CarlesPibernat\AdminGeneratorBundle\Model\Mapper\ListMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;

class AdminDefault extends AbstractAdmin
{
    /**
     * @inheritdoc
     */
    protected function configureFilters()
    {
        // TODO: Implement configureFilters() method.
    }

    /**
     * @inheritdoc
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getEntityManager();
        $className = $this->getClassName();

        $classAccess = new ClassAccess(new $className(), $em);
        $properties = $classAccess->getProperties();

        $metadata = $em->getClassMetadata($className);

        $fieldMappings = $metadata->fieldMappings;
        $associationMappings = $metadata->associationMappings;

        foreach ($fieldMappings as $fieldMapping) {
            $this->addFormField($formMapper, $fieldMapping);
        }

        foreach ($associationMappings as $associationMapping) {
            $this->addAssociationField($formMapper, $associationMapping);
        }
    }

    /**
     * @inheritdoc
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $em = $this->getEntityManager();
        $className = $this->getClassName();

        $classAccess = new ClassAccess(new $className(), $em);
        $properties = $classAccess->getProperties();

        foreach ($properties as $property) {
            $listMapper->add($property);
        }
    }

    private function addFormField(FormMapper $formMapper, array $fieldMapping)
    {
        $options = array();
        $type = TextType::class;

        switch ($fieldMapping['type']) {
            case "integer":
                $type = IntegerType::class;
                break;

            case "boolean":
                $type = CheckboxType::class;
                $options["required"] = false;
                break;

            case "text":
                $type = TextareaType::class;
                break;
        }

        if ($fieldMapping["nullable"]) {
            $options["required"] = false;
        }

        $options['label'] = ucfirst($fieldMapping["fieldName"]);

        if (isset($fieldMapping['id']) && $fieldMapping['id']) {
            $options['disabled'] = true;
        }

        $formMapper->add($fieldMapping['fieldName'], $type, $options);
    }

    private function addAssociationField(FormMapper $formMapper, array $associationMapping)
    {
        $formMapper->add($associationMapping["fieldName"], EntityType::class, array(
            "class" => $associationMapping["targetEntity"],
            'choice_label' => 'title'
        ));
    }
}