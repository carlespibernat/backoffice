<?php
/*
 * This file is part of the AdminGeneratorBundle.
 *
 * Carles Pibernat
 */

namespace CarlesPibernat\AdminGeneratorBundle\Model;

/**
 * Entity configuration
 *
 * @author Carles Pibernat
 */
class EntityConfiguration
{
    /**
     * @var string
     */
    private $class;

    /**
     * @var string
     */
    private $adminClass;

    /**
     * @var string
     */
    private $label;

    /**
     * @var string
     */
    private $entity;

    /**
     * @var string
     */
    private $icon;

    /**
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * @param string $class
     */
    public function setClass(string $class)
    {
        $this->class = $class;
    }

    /**
     * @return string
     */
    public function getAdminClass(): string
    {
        return $this->adminClass;
    }

    /**
     * @param string $adminClass
     */
    public function setAdminClass(string $adminClass)
    {
        $this->adminClass = $adminClass;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel(string $label)
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getEntity(): string
    {
        return $this->entity;
    }

    /**
     * @param string $entity
     */
    public function setEntity(string $entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     */
    public function setIcon(string $icon)
    {
        $this->icon = $icon;
    }
}