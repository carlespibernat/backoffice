<?php

/*
 * This file is part of the AdminGeneratorBundle.
 *
 * Carles Pibernat
 */

namespace CarlesPibernat\AdminGeneratorBundle\Model;
use CarlesPibernat\AdminGeneratorBundle\Model\Mapper\FormMapper;
use CarlesPibernat\AdminGeneratorBundle\Model\Mapper\ListMapper;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container as SymfonyContainer;

/**
 * @author Carles Pibernat
 */
abstract class AbstractAdmin implements AdminInterface
{
    /**
     * @var FormMapper
     */
    private $formMapper;

    /**
     * @var ListMapper
     */
    private $listMapper;
    private $filterMapper;

    /**
     * @var string
     */
    private $className;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var SymfonyContainer
     */
    private $container;

    public function __construct(string $className, EntityManager $em, SymfonyContainer $container)
    {
        $this->listMapper = new ListMapper();
        $this->formMapper = new FormMapper();
        $this->className = $className;
        $this->entityManager = $em;
        $this->container = $container;
    }

    /**
     * @inheritdoc
     */
    public function getForm(): array
    {
        $this->configureFormFields($this->formMapper);
        return $this->formMapper->getMapperItems();
    }

    /**
     * @inheritdoc
     */
    public function getList(): array
    {
        $this->configureListFields($this->listMapper);
        return $this->listMapper->getMapperItems();
    }

    /**
     * @inheritdoc
     */
    public function getFilters(): array
    {
        // TODO: Implement getFilters() method.
    }

    /**
     * Configure entity form fields
     *
     * @param FormMapper $formMapper
     */
    protected abstract function configureFormFields(FormMapper $formMapper);

    /**
     * Configure entity list fields
     *
     * @param ListMapper $listMapper
     */
    protected abstract function configureListFields(ListMapper $listMapper);

    /**
     * Configure entity list filters
     */
    protected abstract function configureFilters();

    /**
     * @inheritdoc
     */
    public function isGranted(): bool
    {
        return true;
    }

    /**
     * Get entity manager
     *
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * Get class name
     *
     * @return string
     */
    protected function getClassName()
    {
        return $this->className;
    }

    /**
     * Actions to be done before update
     *
     * @param $entity
     */
    public function preUpdate($entity)
    {

    }

    /**
     * Actions to be done before create
     *
     * @param $entity
     */
    public function preCreate($entity)
    {

    }

    protected function getContainer(): SymfonyContainer
    {
        return $this->container;
    }
}