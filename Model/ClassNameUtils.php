<?php
/*
 * This file is part of the AdminGeneratorBundle.
 *
 * Carles Pibernat
 */

namespace CarlesPibernat\AdminGeneratorBundle\Model;

/**
 * Class name manipulation methods
 *
 * @author Carles Pibernat
 */
class ClassNameUtils
{
    /**
     * Get class name from a full class name.
     * AppBundle\Entity\MyClass returns MyClass
     *
     * @param string $class
     * @return string
     */
    public static function getClassName(string $class): string
    {
        $explode = explode('\\', $class);

        return $explode[sizeof($explode) - 1];
    }

    /**
     * Get class name from slug
     *
     * @param string $slug
     * @return mixed
     */
    public static function getClassNameFromSlug(string $slug)
    {
        $entity = implode('-', array_map('ucfirst', explode('-', $slug)));
        return str_replace('-', '', $entity);
    }
}