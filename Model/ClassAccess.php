<?php
/*
 * This file is part of the AdminGeneratorBundle.
 *
 * Carles Pibernat
 */

namespace CarlesPibernat\AdminGeneratorBundle\Model;
use Doctrine\ORM\EntityManager;

/**
 * Class private properties access
 *
 * @author Carles Pibernat
 */
class ClassAccess
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var object
     */
    private $classInstance;

    /**
     * @var string
     */
    private $className;

    public function __construct($classInstance, EntityManager $em)
    {
        $this->classInstance = $classInstance;
        $this->entityManager = $em;
        $this->className = get_class($classInstance);
    }

    /**
     * Get class instance property calling it's getter
     *
     * @param string $property
     * @return mixed
     * @throws \Exception
     */
    public function get(string $property)
    {
        $property = ucfirst($property);
        $methods = get_class_methods($this->className);
        
        if (in_array("get$property", $methods)) {
            $method = "get$property";
            return $this->classInstance->$method();
        } else {
            throw new \Exception("No method get$property found in {$this->className}");
        }
    }

    /**
     * Get all class properties
     *
     * @return array
     */
    public function getProperties()
    {
        return $this->entityManager->getClassMetadata($this->className)->getFieldNames();
    }
}