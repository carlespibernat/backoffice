<?php
/*
 * This file is part of the AdminGeneratorBundle.
 *
 * Carles Pibernat
 */

namespace CarlesPibernat\AdminGeneratorBundle\Model\Utils;


class BreadcrumbItem
{
    /**
     * @var string
     */
    private $label;

    /**
     * @var string
     */
    private $path;

    public function __construct(string $label, string $path = '')
    {
        $this->setLabel($label);
        $this->setPath($path);
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    private function setLabel(string $label)
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    private function setPath(string $path)
    {
        $this->path = $path;
    }
}