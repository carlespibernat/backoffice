<?php
/*
 * This file is part of the AdminGeneratorBundle.
 *
 * Carles Pibernat
 */

namespace CarlesPibernat\AdminGeneratorBundle\Model\Utils;

/**
 * Parameters constraint class
 *
 * @author Carles Pibernat
 */
class ParameterConstraint
{
    /**
     * @var array
     */
    private $constraints = array();

    /**
     * @var mixed
     */
    private $defaultValue;

    /**
     * @var bool
     */
    private $isRequired = false;

    const VALID_TYPES = array('array', 'int', 'integer', 'string', 'bool', 'boolean', 'object', "null", "NULL");

    /**
     * Check if the given constraint is a valid one
     *
     * @param string $constraint
     * @return bool
     */
    private function checkValidConstraint(string $constraint)
    {
        $constraint = trim($constraint);
        return in_array($constraint, self::VALID_TYPES);
    }

    /**
     * Set default value
     *
     * @param $value
     */
    public function setDefaultValue($value)
    {
        $this->defaultValue = $value;
    }

    /**
     * Return default value
     *
     * @return mixed
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * Set if this parameter is required
     *
     * @param bool $required
     */
    public function setIsRequired(bool $required = true)
    {
        $this->isRequired = $required;
    }

    /**
     * Check if this parameter is required
     *
     * @return bool
     */
    public function getIsRequired(): bool
    {
        return $this->isRequired;
    }

    /**
     * Returns a string with all valid types separated by commas
     *
     * @return string
     */
    public function getStringWithValidTypes(): string
    {
        return implode(', ', $this->constraints);
    }

    /**
     * Check if the given parameter is a valid one. Returns error string if
     * not valid parameter
     *
     * @param $parameter
     * @return bool|string
     */
    public function checkIsValidParameter($parameter)
    {
        $result = in_array(gettype($parameter), $this->constraints);

        if ($result === true) {
            return true;
        } else {
            return "
                Invalid parameter of type " . gettype($parameter) . ". Valid types are: " .
                $this->getStringWithValidTypes()
            ;
        }
    }

    /**
     * Adds a new constraint
     *
     * @param string $constraint
     * @return ParameterConstraint
     */
    public function addConstraint(string $constraint)
    {
        // Adapt constraint to values returned by gettype method
        if ($constraint == 'int') {
            $constraint = "integer";
        } elseif ($constraint == 'bool') {
            $constraint = "boolean";
        } elseif ($constraint == "null") {
            $constraint = 'NULL';
        }

        if ($this->checkValidConstraint($constraint)) {
            $this->constraints[] = $constraint;
        } else {
            throw new \InvalidArgumentException("
                Invalid constraint $constraint. Available types are: " . implode(', ', self::VALID_TYPES) . "
            ");
        }

        return $this;
    }

}