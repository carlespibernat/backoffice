<?php
/*
 * This file is part of the AdminGeneratorBundle.
 *
 * Carles Pibernat
 */

namespace CarlesPibernat\AdminGeneratorBundle\Model\Utils;

/**
 * Parameters validator class
 *
 * @author Carles Pibernat
 */
class ParametersValidator
{
    /**
     * @var ParameterConstraint[]
     */
    private $constraints = array();

    /**
     * @var ParameterConstraint
     */
    private $currentConstraint;

    /**
     * Creates a new parameter constraint
     *
     * @return ParametersValidator
     */
    public function newParameterConstraint(): ParametersValidator
    {
        $this->currentConstraint = new ParameterConstraint();

        return $this;
    }

    /**
     * Add new constraint to current parameter
     *
     * @param string $constraint
     * @return ParametersValidator
     */
    public function addConstraint(string $constraint): ParametersValidator
    {
        $this->currentConstraint->addConstraint($constraint);

        return $this;
    }

    /**
     * Set default value to current constraint
     *
     * @param $defaultValue
     * @return ParametersValidator
     */
    public function default($defaultValue): ParametersValidator
    {
        $this->currentConstraint->setDefaultValue($defaultValue);

        return $this;
    }

    /**
     * Set that current parameters is required
     *
     * @return ParametersValidator
     */
    public function isRequired(): ParametersValidator
    {
        $this->currentConstraint->setIsRequired();

        return $this;
    }

    /**
     * End current parameter constraint
     *
     * @return ParametersValidator
     */
    public function end(): ParametersValidator
    {
        $this->constraints[] = $this->currentConstraint;

        return $this;
    }

    /**
     * Check if the given parameters are valid. Returns array with all parameters if valid, a string with
     * an error if not
     *
     * @param array $parameters
     * @return array|string
     */
    public function validateAndGetParameters(array $parameters)
    {
        foreach ($this->constraints as $key => $constraint) {
            if (isset($parameters[$key])) {
                $result = $constraint->checkIsValidParameter($parameters[$key]);
                if ($result !== true) {
                    return $result;
                }
            } elseif ($constraint->getIsRequired()) {
                return "Parameter $key of type " . $constraint->getStringWithValidTypes() . " required";
            } else {
                $parameters[$key] = $constraint->getDefaultValue();
            }
        }

        return $parameters;
    }
}