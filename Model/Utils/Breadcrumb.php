<?php
/*
 * This file is part of the AdminGeneratorBundle.
 *
 * Carles Pibernat
 */

namespace CarlesPibernat\AdminGeneratorBundle\Model\Utils;

/**
 * Class used by twig templates to generate a breadcrumb
 *
 * @author Carles Pibernat
 */
class Breadcrumb
{
    /**
     * @var BreadcrumbItem
     */
    private $entityClassLabel;

    /**
     * @var BreadcrumbItem
     */
    private $entityLabel;

    /**
     * @var string
     */
    private $sectionTitle;

    /**
     * @var string
     */
    private $sectionDescription;

    /**
     * @return BreadcrumbItem
     */
    public function getEntityClassLabel()
    {
        return $this->entityClassLabel;
    }

    /**
     * @param BreadcrumbItem $entityClassLabel
     *
     * @return Breadcrumb
     */
    public function setEntityClassLabel(BreadcrumbItem $entityClassLabel)
    {
        $this->entityClassLabel = $entityClassLabel;

        return $this;
    }

    /**
     * @return BreadcrumbItem
     */
    public function getEntityLabel()
    {
        return $this->entityLabel;
    }

    /**
     * @param BreadcrumbItem $entityLabel
     *
     * @return Breadcrumb
     */
    public function setEntityLabel(BreadcrumbItem $entityLabel)
    {
        $this->entityLabel = $entityLabel;

        return $this;
    }

    /**
     * @return string
     */
    public function getSectionTitle()
    {
        return $this->sectionTitle;
    }

    /**
     * @param string $sectionTitle
     */
    public function setSectionTitle(string $sectionTitle)
    {
        $this->sectionTitle = $sectionTitle;
    }

    /**
     * @return string
     */
    public function getSectionDescription()
    {
        return $this->sectionDescription;
    }

    /**
     * @param string $sectionDescription
     */
    public function setSectionDescription(string $sectionDescription)
    {
        $this->sectionDescription = $sectionDescription;
    }
}