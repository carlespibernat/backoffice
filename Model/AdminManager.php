<?php
/*
 * This file is part of the AdminGeneratorBundle.
 *
 * Carles Pibernat
 */

namespace CarlesPibernat\AdminGeneratorBundle\Model;


use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

/**
 * Manage admin classes
 *
 * @author Carles Pibernat
 */
class AdminManager
{
    /**
     * @var UserConfiguration
     */
    private $userConfiguration;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var Container
     */
    private $container;

    public function __construct(UserConfiguration $userConfiguration, EntityManager $entityManager, Container $container)
    {
        $this->userConfiguration = $userConfiguration;
        $this->entityManager = $entityManager;
        $this->container = $container;
    }

    /**
     * Get entity by class name
     *
     * @param string $className
     * @return EntityConfiguration|bool
     */
    public function getEntity(string $className)
    {
        foreach ($this->userConfiguration->getEntities() as $entity) {
            if ($entity->getEntity() == ClassNameUtils::getClassNameFromSlug($className)) {
                return $entity;
            }
        }

        return false;
    }

    /**
     * Get an admin class instance of the given entity
     *
     * @param string $className
     * @return bool|AbstractAdmin
     */
    public function getAdminInstance(string $className)
    {
        $entityConfiguration = $this->getEntity($className);

        if ($entityConfiguration) {
            $adminClass = $entityConfiguration->getAdminClass();
            return new $adminClass($entityConfiguration->getClass(), $this->entityManager, $this->container);
        } else {
            return false;
        }
    }
}