<?php
/*
 * This file is part of the AdminGeneratorBundle.
 *
 * Carles Pibernat
 */

namespace CarlesPibernat\AdminGeneratorBundle\Model;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

/**
 * User configuration
 *
 * @author Carles Pibernat
 */
class UserConfiguration
{
    const DEFAULT_ADMIN_CLASS = 'CarlesPibernat\\AdminGeneratorBundle\\Model\\AdminDefault';

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $titleLogo;

    /**
     * @var string
     */
    protected $uploadDirectory;

    /**
     * @var string
     */
    protected $uploadDirectoryUri;

    /**
     * @var Container
     */
    private $container;

    /**
     * @var EntityConfiguration[]
     */
    private $entities;

    public function __construct(Container $container)
    {
        $this->container = $container;

        $this->parseContainer();
    }

    /**
     * Parse the user configuration from the container
     */
    private function parseContainer()
    {
        $configuration = $this->container->getParameter('admin.generator.configuration');

        // Get title
        $this->setTitle($configuration['title']);

        // Get main logo
        $this->setTitleLogo($configuration['title_logo']);

        // Get upload directory
        $this->setUploadDirectory($configuration['upload_directory']);

        // Get upload directory uri
        $this->setUploadDirectoryUri($configuration['upload_directory_uri']);

        // Get entities
        foreach ($configuration['entities'] as $entityArray) {
            $entity = new EntityConfiguration();

            $entity->setClass($entityArray['class']);
            $entity->setEntity(ClassNameUtils::getClassName($entity->getClass()));

            if ($entityArray['admin_class'])
                $entity->setAdminClass($entityArray['admin_class']);
            else
                $entity->setAdminClass(self::DEFAULT_ADMIN_CLASS);

            if ($entityArray['label'])
                $entity->setLabel($entityArray['label']);
            else
                $entity->setLabel($entity->getEntity());

            $entity->setIcon($entityArray['icon']);

            $this->addEntity($entity);
        }
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    private function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitleLogo(): string
    {
        return $this->titleLogo;
    }

    /**
     * @param string $titleLogo
     */
    private function setTitleLogo(string $titleLogo)
    {
        $this->titleLogo = $titleLogo;
    }

    /**
     * @return string
     */
    public function getUploadDirectory(): string
    {
        return $this->uploadDirectory;
    }

    /**
     * @param string $uploadDirectory
     */
    public function setUploadDirectory(string $uploadDirectory)
    {
        $this->uploadDirectory = $uploadDirectory;
    }

    /**
     * @return string
     */
    public function getUploadDirectoryUri(): string
    {
        return $this->uploadDirectoryUri;
    }

    /**
     * @param string $uploadDirectoryUri
     */
    public function setUploadDirectoryUri(string $uploadDirectoryUri)
    {
        $this->uploadDirectoryUri = $uploadDirectoryUri;
    }

    /**
     * @return EntityConfiguration[]
     */
    public function getEntities(): array
    {
        return $this->entities;
    }

    /**
     * @param EntityConfiguration[] $entities
     */
    private function setEntities(array $entities)
    {
        $this->entities = $entities;
    }

    /**
     * @param EntityConfiguration $entity
     */
    private function addEntity(EntityConfiguration $entity)
    {
        $this->entities[] = $entity;
    }
}