<?php
/*
 * This file is part of the AdminGeneratorBundle.
 *
 * Carles Pibernat
 */

namespace CarlesPibernat\AdminGeneratorBundle\Model;

/**
 * @author Carles Pibernat
 */
interface AdminInterface
{
    /**
     * Get form an array of form fields
     *
     * @return array
     */
    public function getForm(): array;

    /**
     * Get an array with the list page columns
     *
     * @return array
     */
    public function getList(): array;

    /**
     * Get the available filters on list page
     *
     * @return array
     */
    public function getFilters(): array;

    /**
     * Check if the user is granted
     *
     * @return bool
     */
    public function isGranted(): bool;
}