<?php
/*
 * This file is part of the AdminGeneratorBundle.
 *
 * Carles Pibernat
 */

namespace CarlesPibernat\AdminGeneratorBundle\Model\Mapper;

/**
 * List mapper item
 *
 * @author Carles Pibernat
 */
class ListMapperItem extends AbstractMapperItem
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var array
     */
    private $options;

    public function __construct(string $name, array $options = array())
    {
        $this->setName($name);
        $this->setOptions($options);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param array $options
     */
    public function setOptions(array $options)
    {
        $this->options = $options;
    }
}