<?php
/*
 * This file is part of the AdminGeneratorBundle.
 *
 * Carles Pibernat
 */

namespace CarlesPibernat\AdminGeneratorBundle\Model\Mapper;
use CarlesPibernat\AdminGeneratorBundle\Model\Utils\ParametersValidator;

/**
 * List mapper
 *
 * @author Carles Pibernat
 */
class ListMapper extends AbstractMapper
{

    /**
     * @inheritdoc
     */
    protected final function getParametersValidator(): ParametersValidator
    {
        $parametersValidator = new ParametersValidator();

        return $parametersValidator
            ->newParameterConstraint()
                ->addConstraint('string')
                ->isRequired()
            ->end()
                ->newParameterConstraint()
                ->addConstraint('array')
                ->default(array())
            ->end()
        ;
    }

    /**
     * @inheritdoc
     */
    protected final function getMapperItemClass(): string
    {
        return ListMapperItem::class;
    }
}