<?php
/**
 * Created by PhpStorm.
 * User: carles
 * Date: 6/4/17
 * Time: 20:45
 */

namespace CarlesPibernat\AdminGeneratorBundle\Model\Mapper;


use CarlesPibernat\AdminGeneratorBundle\Model\Utils\ParametersValidator;

abstract class AbstractMapper
{
    /**
     * @var AbstractMapperItem[]
     */
    private $mapperItems;

    public function __construct()
    {
        $this->mapperItems = array();
    }

    /**
     * @return array
     */
    public function getMapperItems()
    {
        return $this->mapperItems;
    }

    /**
     * @param mixed $mapperItem
     */
    protected function addMapperItem(AbstractMapperItem $mapperItem)
    {
        $this->mapperItems[] = $mapperItem;
    }

    /**
     * Public add mapper item
     *
     * @return AbstractMapper
     */
    public function add()
    {
        $parameters = func_get_args();
        $parametersValidator = $this->getParametersValidator();

        $parameters = $parametersValidator->validateAndGetParameters($parameters);

        if (gettype($parameters) == 'string') {
            throw new \InvalidArgumentException($parameters);
        }

        $class = $this->getMapperItemClass();

        $reflect  = new \ReflectionClass($class);
        $mapperItem = $reflect->newInstanceArgs($parameters);

        $this->addMapperItem($mapperItem);

        return $this;
    }

    /**
     * Get add method parameters validator
     *
     * @return ParametersValidator
     */
    protected abstract function getParametersValidator(): ParametersValidator;

    /**
     * Get mapper item class name
     *
     * @return string
     */
    protected abstract function getMapperItemClass(): string;
}