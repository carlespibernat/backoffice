<?php

namespace CarlesPibernat\AdminGeneratorBundle\Model\Mapper;

use CarlesPibernat\AdminGeneratorBundle\Model\Utils\ParametersValidator;

class FormMapper extends AbstractMapper
{
    /**
     * @inheritdoc
     */
    protected final function getParametersValidator(): ParametersValidator
    {
        $parametersValidator = new ParametersValidator();

        return $parametersValidator
            ->newParameterConstraint()
                ->addConstraint('string')
                ->isRequired()
            ->end()
            ->newParameterConstraint()
                ->addConstraint('string')
            ->end()
            ->newParameterConstraint()
                ->addConstraint('array')
                ->default(array())
            ->end()
        ;
    }

    /**
     * @inheritdoc
     */
    protected final function getMapperItemClass(): string
    {
        return FormMapperItem::class;
    }
}