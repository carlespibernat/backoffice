
AdminGeneratorBundle
===================

This bundle provides a simple way to geneate an admin for a Symfony web application.

----------


Installation and configuration
-------------

The installation of the bundle can be easily done using [Composer](https://getcomposer.org/). This package has the following dependencies that will also be installed if not exists:

 - [Symfony Standard Edition](https://github.com/symfony/symfony-standard)
 - [KnpPaginatorBundle](https://github.com/KnpLabs/KnpPaginatorBundle)
 - [FOSUserBUndle](https://github.com/FriendsOfSymfony/FOSUserBundle)
 - [KnpMenuBundle](https://github.com/KnpLabs/KnpMenuBundle)
 - [LiipImagineBundle](https://github.com/liip/LiipImagineBundle)
 - [cocur/slugify](https://github.com/cocur/slugify)

If you don't have Composer installed in your computer, start by [installing Composer globally](https://getcomposer.org/download/). Then execute the `create-project` command to create a new Symfony application including the admin generator.

    composer create-project carles-pibernat/admin-generator-bundle
    
Now to update the composer run:

    composer update
    
Now configure the FOSUserBundle. Add the following lines to the config.xml file:

    # FOSUserBundle
    fos_user:
        db_driver: orm
        firewall_name: main
        user_class: CarlesPibernat\AdminGeneratorBundle\Entity\User
        
Copy the following lines and replace the security.yml content:

    # To get started with security, check out the documentation:
    # http://symfony.com/doc/current/security.html
    security:
    
        # FOSUserBundle
        encoders:
            FOS\UserBundle\Model\UserInterface: bcrypt
    
        role_hierarchy:
            ROLE_ADMIN:       ROLE_USER
            ROLE_SUPER_ADMIN: ROLE_ADMIN
    
        providers:
            fos_userbundle:
                id: fos_user.user_provider.username
    
        firewalls:
            main:
                pattern: /admin(.*)
                form_login:
                    provider: fos_userbundle
                    csrf_token_generator: security.csrf.token_manager
                    login_path:     /admin/login
                    check_path:     /admin/login_check
                    default_target_path: /admin/
    
                logout:
                    path:           /admin/logout
                    target:         /admin/login
                anonymous:    true
    
        access_control:
            - { path: ^/login$, role: IS_AUTHENTICATED_ANONYMOUSLY }
            - { path: ^/register, role: IS_AUTHENTICATED_ANONYMOUSLY }
            - { path: ^/resetting, role: IS_AUTHENTICATED_ANONYMOUSLY }
            - { path: ^/admin/login$, role: IS_AUTHENTICATED_ANONYMOUSLY }
            - { path: ^/admin/logout$, role: IS_AUTHENTICATED_ANONYMOUSLY }
            - { path: ^/admin/login_check$, role: IS_AUTHENTICATED_ANONYMOUSLY }
            - { path: ^/admin, role: ROLE_ADMIN }
        
To finish the installation add the FOSUserBundle and AdminGeneratorBundle routing files to the routing.yml file:

    # FOSUserBundle
    fos_user:
        resource: "@FOSUserBundle/Resources/config/routing/all.xml"
        prefix: /admin
    
    # AdminGeneratorBundle
    admin_area:
        resource: "@AdminGeneratorBundle/Resources/config/admin_generator.yml"
        prefix: /admin