<?php

namespace CarlesPibernat\AdminGeneratorBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AdminGeneratorBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
