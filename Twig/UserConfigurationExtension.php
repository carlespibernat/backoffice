<?php

namespace CarlesPibernat\AdminGeneratorBundle\Twig;

/*
 * This file is part of the AdminGeneratorBundle.
 *
 * Carles Pibernat
 */
use CarlesPibernat\AdminGeneratorBundle\Entity\Media;
use CarlesPibernat\AdminGeneratorBundle\Model\UserConfiguration;

/**
 * Make admin generator user configuration visible from twig templates
 *
 * @author Carles Pibernat
 */
class UserConfigurationExtension extends \Twig_Extension
{
    /**
     * @var UserConfiguration
     */
    private $userConfiguration;

    public function __construct(UserConfiguration $userConfiguration)
    {
        $this->userConfiguration = $userConfiguration;
    }

    public function getGlobals()
    {
        return array(
            'userConfiguration' => $this->userConfiguration
        );
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('getMediaPath', array($this, 'getMediaPath'))
        );
    }

    public function getMediaPath(Media $media)
    {
        return $this->userConfiguration->getUploadDirectoryUri() . $media->getFileName();
    }
}